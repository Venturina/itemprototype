#ifndef _Player_
#define _Player_
#pragma once
#include <list>
#include <map>
#include "Item.h"
#include "ItemStorage.h"
#include "Monster.h"


class Player
{
public:
	Player(ItemStorage& storage, Monster& m);
	~Player(void);

	//returns amount of items of type Item in invenory
	int inventory_add_item(Item item);

	//deletes an item, if its usable
	int inventory_delete_item(Item item);

	//prints inventory to console
	void inventory_show();

	//uses an item
	void use(Item item);

	void wear_armor(Item item);
	void wear_weapon(Item item);

	//first int is Item id, second int is amount
	std::map<int, int> _inventory;

	//deals _attack dmg to monster
	void Player::attack();

	//get attackt by monster
	void get_attacked();
	           
	int _attack;
	int _life;
	int _mana;
	int _armor;

	Item _armorItem;
	Item _weaponItem;

	ItemStorage& _storage;
	Monster& _monster;
};

#endif

