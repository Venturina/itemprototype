#include <fstream>
#include <conio.h> 
#include <iostream>
#include <vector>
#include "Player.h"
#include "ItemStorage.h"
#include "Effect.h"
#include "rapidxml/rapidxml.hpp"
using namespace std;

int main(int argc, char *argv[])
{
    fstream f;
    char cstring[256];
	std::string ar[3];
	int id = 0;
    f.open("items.txt", ios::in);
	int c = 0;
	ItemStorage storage;

	//std::vector<Item>::iterator it = vectoritem.begin();
	cout << "Einlesen Items" << endl;
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<> * root_node;
	// Read the xml file into a vector
	ifstream theFile("items.xml");
	std::vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
	buffer.push_back('\0');
	// Parse the buffer using the xml file parsing library into doc
	doc.parse<0>(&buffer[0]);
	// Find our root node
	root_node = doc.first_node("Items");
	// Iterate over items
	for (rapidxml::xml_node<> * item_node = root_node->first_node("Item"); item_node; item_node = item_node->next_sibling())
	{
		std::map<int, int> usedTo;
		for (rapidxml::xml_node<> * aktion_node = item_node->first_node("Aktion"); aktion_node; aktion_node = aktion_node->next_sibling()) {		
			usedTo[std::atoi(aktion_node->first_attribute("ID")->value())] = std::atoi(aktion_node->value());
		}
		Item item(std::atoi(item_node->first_attribute("ID")->value()), item_node->first_attribute("name")->value(), atoi(item_node->first_attribute("benutzbar")->value()), item_node->first_attribute("beschreibung")->value(), usedTo);
		storage._map[std::atoi(item_node->first_attribute("ID")->value())]=item;
	}
////-------------------code to read from txt file--------------------------------------

	Monster m;
	Player p(storage, m);

	p.inventory_add_item(storage._map.find(1)->second);
	p.inventory_add_item(storage._map.find(1)->second);
	p.inventory_add_item(storage._map.find(1)->second);
	p.inventory_add_item(storage._map.find(2)->second);
	p.inventory_add_item(storage._map.find(4)->second);
	p.inventory_add_item(storage._map.find(5)->second);
	p.wear_weapon(storage._map.find(2)->second);
	//p.wear_armor(storage._map.find(4)->second);
	while(p._life > 0) {
		std::cout << "____________________________________________________________" << std::endl;
		std::cout << "Its your turn" << std::endl;
		std::cout << "Press 1 for using an Item" << std::endl;
		std::cout << "Press 2 for changing armor" << std::endl;
		std::cout << "Press 3 for changing weapon" << std::endl;
		std::cout << "Press any key to attack" << std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;
		char a = getch();
		switch (a)
		{
		case '1':
			std::cout << "you chose using an Item" << std::endl;
			std::cout << "which one?" << std::endl;
			std::cout << std::endl;
			p.inventory_show();
			a = getch();
			p.use(storage._map.find(a-'0')->second);
			break;
		case '2':
			std::cout << "you chose changing your armor" << std::endl;
			std::cout << "to which one?" << std::endl;
			std::cout << std::endl;
			p.inventory_show();
			a = getch();
			p.wear_armor(storage._map.find(a-'0')->second);
			break;
		case '3':
			std::cout << "you chose changing your sword" << std::endl;
			std::cout << "to which one?" << std::endl;
			std::cout << std::endl;
			p.inventory_show();
			a = getch();
			p.wear_weapon(storage._map.find(a-'0')->second);
			break;
		default:
			break;
		}

		p.attack();
		p.get_attacked();
		if(p._monster._life <= 0) {
			p._monster._life = 100;
			p._monster.drop(p);
		}
	}
	getch();

}