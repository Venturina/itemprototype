#include "Effect.h"
#include <cstring>
#include <iostream>
#include <vector>
#include "iterator" 
#include "rapidxml/rapidxml.hpp"

Effect::Effect(void)
{
}

void Effect::do_effect(Item item, Player* p1, Monster* m){
	for(auto elem : item._effects) {
		switch (elem.first)
		{
		case 1:  //heal yourself
		p1->_life += elem.second;
		break;
	case 2: //damage a enemy
		m->_life -= elem.second;
		break;
	case 3: //damage yourself
		p1->_life -= elem.second;
		break;
	case 4: //add armor points
		p1->_armor += elem.second;
		break;
	case 5: //add attack points
		p1->_attack += elem.second;
		break;
	default:
		break;
		}
	}
	std::cout << "After use of " << item._name << std::endl;
	std::cout << "monster life: " << int(m->_life) << std::endl;
	std::cout << "player life: " << int(p1->_life) << " player mana: " << int(p1->_mana) << " player armor: " << int(p1->_armor) << " player attack: " << int(p1->_attack)  << std::endl;
}

void Effect::remove_effect(Item item, Player* p1, Monster* m){
	for(auto elem : item._effects) {
		switch (elem.first)
		{
		case 1:  //heal yourself
		p1->_life -= elem.second;
		break;
	case 2: //damage a enemy
		m->_life += elem.second;
		break;
	case 3: //damage yourself
		p1->_life += elem.second;
		break;
	case 4: //add armor points
		p1->_armor -= elem.second;
		break;
	case 5: //add attack points
		p1->_attack -= elem.second;
		break;
	default:
		break;
		}
	}

	std::cout << "Removing Effect of " << item._name << std::endl;
	std::cout << "monster life: " << int(m->_life) << std::endl;
	std::cout << "player life: " << int(p1->_life) << " player mana: " << int(p1->_mana) << " player armor: " << int(p1->_armor) << " player attack: " << int(p1->_attack) << std::endl;
}

Effect::~Effect(void)
{
}
