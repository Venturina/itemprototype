#ifndef _Effect_
#define _Effect_

#include "Player.h"
#include "Item.h"
#include "Monster.h"

#pragma once
class Effect
{
public:
	Effect(void);
	~Effect(void);

	//assignes a effect value to an item, and applys this effect
	void do_effect(Item item, Player* p1, Monster* p2);

	void remove_effect(Item item, Player* p1, Monster* m);
};

#endif