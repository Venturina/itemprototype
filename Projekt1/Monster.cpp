#include "Monster.h"
#include <cstdlib>
#include "Player.h"


Monster::Monster(void):
	_life(100), _attack(30)
{
}

Monster& Monster::operator = (const Monster& src){
	_life = src._life;
	_attack = src._attack;
	return *this;
}

void Monster::drop(Player& p) {
	int count = p._storage._map.size();
	int id = rand() % count;
	p.inventory_add_item(p._storage._map.find(id)->second);
}

Monster::~Monster(void)
{
}
