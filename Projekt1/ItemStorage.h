#ifndef _ItemStorage_
#define _ItemStorage_

#pragma once
#include <list>
#include <map>
#include "item.h"
class ItemStorage
{
public:
	ItemStorage(void);
	~ItemStorage(void);

	//adds an item red from txt file to the item list
	void addItem(Item item);
	 
	//holds an object of each item
	std::map<int, Item> _map;
};

#endif