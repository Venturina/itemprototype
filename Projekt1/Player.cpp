#include "Player.h"
#include "Effect.h"
#include <iostream>


Player::Player(ItemStorage& storage, Monster& m):
	_storage(storage), _life(100), _mana(100), _monster(m), _armor(0), _attack(0)
{
}
int Player::inventory_add_item(Item item) {
	bool contains = false;
	if(_inventory.count(item._id) == 0) {
		_inventory[item._id] = 1;
		std::cout << "After adding " << item._name << " to inventory, its in there 1 time!" << std::endl;
		return 1;
	} else {
		int tmp = _inventory.at(item._id);
		_inventory[item._id] = tmp + 1;
		std::cout << "After adding " << item._name << " to inventory, its in there " << tmp+1 << " times!" << std::endl;
		return tmp+1;
	}	
}


int Player::inventory_delete_item(Item item){
	if(_inventory.count(item._id) == 0) {
		return -1;
	} else if(item._useable == 1){
		int tmp = _inventory.at(item._id);
		_inventory[item._id] = tmp - 1;
		if(tmp == 1){
			_inventory.erase(item._id);
		}
		return tmp-1;
	}
}

void Player::inventory_show(){
	std::cout << "Inventory contains:" << std::endl;
	for(auto elem : _inventory) {
		std::cout << "Name: " << _storage._map[elem.first]._name << " Item ID: " << elem.first << " amount: " << elem.second <<  std::endl;
	}
}

void Player::use(Item item){
	if(inventory_delete_item(item) < 0){
		std::cout << "no such item there o.O" << std::endl;
	} else if(item._useable == 1){
		Effect ef;
		ef.do_effect(item, this, &_monster);
	} else {
		std::cout << "You tried to use a nonuseable Item" << std::endl;
	}
}

void Player::wear_armor(Item item) {
	Effect ef;
	if(inventory_delete_item(item) >= 0 && item._useable == 3) {
		if(_armor == 0){
			ef.do_effect(item, this, &_monster);
			_armorItem = item;
		}else {
			ef.remove_effect(_armorItem, this, &_monster);
			ef.do_effect(item, this, &_monster);
			_armorItem = item;
		}
	} else if (item._useable != 3) {
		std::cout << "This is no armor to wear" << std::endl;
	}
}

void Player::wear_weapon(Item item) {
	Effect ef;
	if(inventory_delete_item(item) >= 0 && item._useable == 2) {
		if(_attack == 0){
			ef.do_effect(item, this, &_monster);
			_weaponItem = item;
		} else {
			ef.remove_effect(_weaponItem, this, &_monster);
			ef.do_effect(item, this, &_monster);
			_weaponItem = item;
		}
	} else if (item._useable != 2) {
		std::cout << "This is no weapon to use" << std::endl;
	}
}

void Player::attack() {
	_monster._life -= _attack;
	std::cout << ">>>>> Monster damaged: Heath left: " << _monster._life << std::endl;
}

void Player::get_attacked() {
	_life -= (_monster._attack - _armor);
	std::cout << ">>>>> Player damaged: Heath left: " << _life << std::endl;
}


Player::~Player(void)
{
}
