#ifndef _Item_
#define _Item_

#include <string>
#include <map>

class Item
{
public:
	Item(void);
	Item(int id, std::string name, int useable, std::string description, std::map<int, int> usedTo);
	~Item(void);

	//item id
	int _id;

	//item name
	std::string _name;

	//is the item useable or not?
	int _useable;

	//item effect
	std::string _description;

	std::map<int, int> _effects;
	
};

#endif
