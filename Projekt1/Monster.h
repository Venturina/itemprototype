#ifndef _Monster_
#define _Monster_

//#include "Player.h"
class Player;

#pragma once
class Monster
{
public:
	Monster(void);
	~Monster(void);

	//monster health, at the moment default set to 100
	int _life;
	int _attack;

	Monster& Monster::operator = (const Monster& src);

	void drop(Player& p);
};

#endif

